package helper;

import java.sql.Timestamp;


public class DateTime {
  public static int getDateIndex() {
    return 1;
  }

  public static boolean twentyFourHours(String[] date) {
    long dateMilliseconds = Timestamp.valueOf(date[DateTime.getDateIndex()] + ":00").getTime();
    long beginTime = System.currentTimeMillis();
    long endTime = beginTime + 86400000;
    return dateMilliseconds >= beginTime && dateMilliseconds <= endTime;
  }

  public static boolean flightsByDay(String[] date, String passengerDate) {
    long dateMilliseconds = Timestamp.valueOf(date[DateTime.getDateIndex()] + ":00").getTime();
    long beginTime = Timestamp.valueOf(passengerDate + " 00:00:00").getTime();
    long endTime = beginTime + 86400000;
    return dateMilliseconds >= beginTime && dateMilliseconds <= endTime;
  }

  public static int comparator(String[] start, String[] end) {
      long  res = Timestamp.valueOf(start[DateTime.getDateIndex()] + ":00").getTime() - Timestamp.valueOf(end[DateTime.getDateIndex()] + ":00").getTime();
      if(res > 0){
        return 1;
      } else if(res < 0) {
        return -1;
      } else {
        return  0;
      }
  }
}
