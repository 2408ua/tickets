package helper;

public class ScoreboardCorrection {

  public static String emptyStringLength(int count){
    StringBuilder tmp = new StringBuilder();
    while(count > 0){
      tmp.append(" ");
      count--;
    }
    return  tmp.toString();
  }

  public static String RowLength(String str){
    int rowLength = 18;
    int ln = rowLength - str.length();
    if( ln > 0){
      return str + emptyStringLength(ln);
    }
    return str;
  }

  public static String FlightN(String str){
    int rowLength = 6;
    int ln = rowLength - str.length();
    if( ln > 0){
      return str + emptyStringLength(ln);
    }
    return str;
  }
}
