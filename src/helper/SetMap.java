package helper;

import java.util.HashMap;
import java.util.Map;

public class SetMap {
  public static Map run(String controller, String method) {
    Map<String, String> map = new HashMap<>();
    map.put("controller", controller);
    map.put("method", method);
    map.put("run", "true");
    return  map;
  }

  public static Map exit() {
    Map<String, String> map = new HashMap<>();
    map.put("run", "false");
    return  map;
  }
}
