package services;

import dao.DaoFile;
import db.Path;

import java.util.*;

public class Booking {

  public static void writBookingFile(List data) {
    String bookings = DaoFile.read(Path.getPath() + "booking");

    if (bookings.equals("Error")) {
      DaoFile.write(Path.getPath() + "booking", "1," + String.join(",", data) + ";");
      return;
    }

    String[][] flightsArr = Arrays.stream(bookings
        .split(";"))
        .map(item -> item.split(","))
        .toArray(String[][]::new);

    Integer lastID = Integer.parseInt(flightsArr[flightsArr.length - 1][0]);
    DaoFile.write(Path.getPath() + "booking",
        bookings +
            (lastID == 1 ? "" : ";") +
            ++lastID +
            "," + String.join(",", data));
  }


  public static List getFlightsIdByPassengerName(String name) {
    String bookings = DaoFile.read(Path.getPath() + "booking");
    List<String> res = new ArrayList<>();
    String[][] tmp = Arrays.stream(bookings
        .split(";"))
        .map(item -> item.split(","))
        .filter(item -> {
          for (String val : item) {
            if (val.equals(name)) return true;
          }
          return false;
        })
        .toArray(String[][]::new);

    for (String[] val : tmp) {
      res.add(val[val.length - 1]);
    }

    return res;
  }

  public static boolean deleteBooking(Map data) {
    String bookings = DaoFile.read(Path.getPath() + "booking");
    Integer bookingsLength = bookings.split(";").length;
    String[][] tmp = Arrays.stream(bookings
        .split(";"))
        .map(item -> item.split(","))
        .filter(item -> {
          for (String val : item) {
            if (val.equals(data.get("name")) && item[item.length - 1].equals(data.get("flightID"))) return false;
          }
          return true;
        })
        .toArray(String[][]::new);

    if (bookingsLength == tmp.length) return false;

    List<String> res = new ArrayList<>();
    for (String[] val : tmp) {
      res.add(String.join(",", val));
    }
    DaoFile.write(Path.getPath() + "booking", String.join(";", res));
    return true;
  }
}
