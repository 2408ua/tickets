package services;

import helper.DateTime;
import dao.DaoFile;
import db.Path;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

public class Flights {

  public static String[][] getFlightsNextTwentyFourHours() {
    String flights = DaoFile.read(Path.getPath() + "flights");
    return Arrays.stream(flights
        .split(";"))
        .map(item -> item.split(","))
        .filter(DateTime::twentyFourHours)
        .sorted(DateTime::comparator)
        .toArray(String[][]::new);
  }

  public static String[][] getFlightById(String id) {
    if (id.equals("0")) return new String[][]{};

    String flights = DaoFile.read(Path.getPath() + "flights");
    return Arrays.stream(flights
        .split(";"))
        .map(item -> item.split(","))
        .filter(item -> item[0].equals(id.trim()))
        .toArray(String[][]::new);
  }

  public static String[][] getFlightByIds(List ids) {
    String flights = DaoFile.read(Path.getPath() + "flights");
    return Arrays.stream(flights
        .split(";"))
        .map(item -> item.split(","))
        .filter(item -> ids.indexOf(item[0]) != -1 )
        .sorted(DateTime::comparator)
        .toArray(String[][]::new);
  }

  public static String[][] getFlightsForBooking(Map data) {
    String flights = DaoFile.read(Path.getPath() + "flights");

    return Arrays.stream(flights
        .split(";"))
        .map(item -> item.split(","))
        .filter(item -> item[2].toLowerCase().equals(data.get("city").toString().toLowerCase().trim())
            && Integer.parseInt(item[4]) >= Integer.parseInt(data.get("countOfPassenger").toString().trim())
            && DateTime.flightsByDay(item, data.get("date").toString().trim())
        )
        .sorted(DateTime::comparator)
        .toArray(String[][]::new);
  }
}
