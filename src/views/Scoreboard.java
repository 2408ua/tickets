package views;

import helper.ScoreboardCorrection;

public class Scoreboard {
  public static void showFlights(String[][] data) {
    System.out.println("********************************************************************************************");
    System.out.println("| №      | Date             | City               | Company            | Free tickets       |");
    System.out.println("********************************************************************************************");
    for (int i = 0, ln = data.length; i < ln; i++) {
      System.out.println("| "
          + ScoreboardCorrection.FlightN(data[i][0])
          + " | " + data[i][1]
          + " | " + ScoreboardCorrection.RowLength(data[i][2])
          + " | " + ScoreboardCorrection.RowLength(data[i][3])
          + " | " + ScoreboardCorrection.RowLength(data[i][4]) + " |");
      System.out.println("--------------------------------------------------------------------------------------------");
    }
  }
}
