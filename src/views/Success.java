package views;

public class Success {
  public static void exit(){
    System.out.println("---------------------------------------------");
    System.out.println("---- Thanks for use our booking program! ----");
    System.out.println("---------------------------------------------");
  }

  public static void bookingAdd(){
    System.out.println("---- Thanks for booking tickets! ----\n");
  }

  public static void bookingDelete(){
    System.out.println("---- Booking has been deleted! ----\n");
  }
}
