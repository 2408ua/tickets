package views;

import java.util.*;

public class Menu {

  public static String scanner(String str) {
    Scanner data = new Scanner(System.in);
    System.out.println(str);
    return data.nextLine().toLowerCase().trim();
  }

  public static String mainMenu() {
    return Menu.scanner(
        "All flights next 24 hours - 5, " +
            "Flight's info by № - 4, " +
            "Booking flight - 3,\n" +
            "Delete booking - 2, " +
            "My flights - 1, " +
            "Exit - 0.\n" +
            "Enter number: ");
  }

  public static String flightByIdMenu() {
    return Menu.scanner("Enter flight's № or 0 for back to main Menu: ");
  }

  public static Map findFlightsForBookingMenu() {
    Map<String, String> tmp = new HashMap<>();
    tmp.put("city", Menu.scanner("Enter city: "));
    tmp.put("date", Menu.scanner("Enter date in format yyyy-MM-dd: "));

    tmp.put("countOfPassenger", Menu.scanner("Enter count of passenger: "));
    return tmp;
  }

  public static List bookingMenu() {
    List<String> tmp = new ArrayList<>();
    String id = Menu.scanner("Enter Flight's №: ");
    do {
      Scanner data = new Scanner(System.in);
      System.out.println("Enter passenger's full name or 0 for booking: ");
      String name = data.nextLine().toLowerCase();
      if (name.equals("0")) break;
      tmp.add(name);
    } while (true);
    tmp.add(id);
    return tmp;
  }

  public static String bookingsByPassengerNameMenu() {
    return Menu.scanner("Enter passenger's name: ");
  }

  public static Map deleteBookingMenu() {
    Map<String, String> tmp = new HashMap<>();
    tmp.put("name", Menu.scanner("Enter full name: "));
    tmp.put("flightID", Menu.scanner("Enter flight №: "));
    return tmp;
  }
}
