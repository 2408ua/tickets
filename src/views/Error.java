package views;

import java.util.Scanner;

public class Error {
  public static void inMainMenu(){
    System.out.println("-------------------------------------");
    System.out.println("--- Incorrect command! Try egan. ----");
    System.out.println("-------------------------------------");
  }

  public static void inFlightById(){
    System.out.println("Incorrect command or Flight №  doesn't exist! Try egan.");
  }

  public static void findFlights(){
    System.out.println("Incorrect data or flights  don't exist! Try egan.");
  }

  public static String bookingFlights(){
    Scanner data = new Scanner(System.in);
    System.out.println("Incorrect Flight's № or tickets don't exist!\n Enter 1 for try egan or 0 for back to main Menu: ");
    return data.nextLine().toLowerCase();
  }

  public static void bookingsByPassengerName(){
    System.out.println("Incorrect name or bookings don't exist!");
  }

  public static void deleteBooking(){
    System.out.println("Incorrect name or booking doesn't exist!");
  }
}
