package controllers;

import helper.SetMap;
import views.Error;
import views.Menu;
import views.Scoreboard;

import java.util.Map;

public class Flights {

  public static Map flightById() {
    String action = Menu.flightByIdMenu();
    String[][] flight = services.Flights.getFlightById(action);

    if (flight.length > 0) action = "flight ID exist";

    switch (action) {
      case "flight ID exist":
        Scoreboard.showFlights(flight);
        return SetMap.run("Flights", "flightById");
      case "0":
        return SetMap.run("Menu", "mainMenu");
      default:
        Error.inFlightById();
        return SetMap.run("Flights", "flightById");
    }
  }

  public static Map findFlightsForBooking() {
    Map data = Menu.findFlightsForBookingMenu();
    String[][] flights = services.Flights.getFlightsForBooking(data);

    if (flights.length > 0) {
      Scoreboard.showFlights(flights);
      return SetMap.run("Booking", "bookingFlight");
    }
    Error.findFlights();
    return SetMap.run("Menu", "mainMenu");
  }
}
