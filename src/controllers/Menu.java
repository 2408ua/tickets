package controllers;

import helper.SetMap;
import services.Flights;
import views.Error;
import views.Scoreboard;

import java.util.Map;

public class Menu {

  public static Map mainMenu() {
    String action = views.Menu.mainMenu();
    switch (action) {
      case "5":
        Scoreboard.showFlights(Flights.getFlightsNextTwentyFourHours());
        return SetMap.run("Menu", "mainMenu");
      case "4":
        return SetMap.run("Flights", "flightById");
      case "3":
        return SetMap.run("Flights", "findFlightsForBooking");
      case "2":
        return SetMap.run("Booking", "deleteBooking");
      case "1":
        return SetMap.run("Booking", "bookingsByPassengerName");
      case "0":
        return SetMap.run("Exit", "exit");
      default:
        Error.inMainMenu();
        return SetMap.run("Menu", "mainMenu");
    }
  }
}
