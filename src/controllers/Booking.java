package controllers;

import helper.SetMap;
import services.Flights;
import views.Error;
import views.Menu;
import views.Scoreboard;
import views.Success;

import java.util.List;
import java.util.Map;

public class Booking {

  public static Map bookingFlight() {
    List data = Menu.bookingMenu();
    String flightID = data.get(data.size() - 1).toString().trim();
    Integer countOfPassengers = data.size() - 1;
    String[][] flight = Flights.getFlightById(flightID);

    if (countOfPassengers > 0 && flight.length > 0 && Integer.parseInt(flight[0][flight[0].length - 1]) >= countOfPassengers) {
      services.Booking.writBookingFile(data);
      Success.bookingAdd();
      return SetMap.run("Menu", "mainMenu");
    }

    if (Error.bookingFlights().equals("1")) {
      return SetMap.run("Booking", "bookingFlight");
    }

    return SetMap.run("Menu", "mainMenu");
  }

  public static Map bookingsByPassengerName() {
    List flights = services.Booking.getFlightsIdByPassengerName(Menu.bookingsByPassengerNameMenu());

    if (flights.size() > 0) {
      Scoreboard.showFlights(Flights.getFlightByIds(flights));
      return SetMap.run("Menu", "mainMenu");
    }

    Error.bookingsByPassengerName();
    return SetMap.run("Menu", "mainMenu");
  }

  public  static Map deleteBooking(){

    if(services.Booking.deleteBooking(Menu.deleteBookingMenu())){
      Success.bookingDelete();
      return SetMap.run("Menu", "mainMenu");
    }

    Error.deleteBooking();
    return SetMap.run("Menu", "mainMenu");
  }
}
