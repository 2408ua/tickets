package dao;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DaoFile {

  public static String read(String src){
    try {
      FileInputStream is = new FileInputStream(src);
      BufferedReader buf = new BufferedReader(new InputStreamReader(is));
      return  buf.readLine();
    } catch (IOException ex) {
      //System.out.println(ex.getMessage());
      return "Error";
    }
  }

  public static void write(String src, String content){
    try
    {
      Files.write(Paths.get(src), content.getBytes());
    }
    catch(IOException ex){
      System.out.println(ex.getMessage());
    }
  }
}
