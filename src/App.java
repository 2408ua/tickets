import java.lang.reflect.*;
import java.util.*;

public class App {
  private Map<String, String> map = new HashMap<>();

  App() {
    map.put("controller", "Menu");
    map.put("method", "mainMenu");
    map.put("run", "true");
  }

  private Map<String, String> getMap() {
    return map;
  }

  private void setMap(Map<String, String> map) {
    this.map = map;
  }

  private boolean isRun() {
    return this.map.get("run").equals("true");
  }

  public void run() {
    try {
      App program = new App();

      while (program.isRun()) {
        Class<?> clazz = Class.forName("controllers." + program.getMap().get("controller"));
        Method method = clazz.getMethod(program.getMap().get("method"));
        program.setMap((Map) method.invoke(clazz, null));
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}

