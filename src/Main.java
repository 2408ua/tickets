import db.DbGenerate;

public class Main {

  public static void main(String[] args) {
    DbGenerate db = new DbGenerate(",");
    db.createFlightsFile();

    App app = new App();
    app.run();
  }
}
