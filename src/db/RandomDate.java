package db;

import java.text.SimpleDateFormat;

public class RandomDate {
  private long beginTime;
  private long endTime;

  RandomDate() {
    beginTime = System.currentTimeMillis();
    endTime = beginTime + 314496 * (long) Math.pow(10, 5);
  }

  private long getRandomTimeBetweenTwoDates() {
    long diff = endTime - beginTime + 1;
    return beginTime + (long) (Math.random() * diff);
  }

  public String generateRandomDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(getRandomTimeBetweenTwoDates());
  }
}
