package db;

import dao.DaoFile;

public class DbGenerate {
  private final int maxCountOfTickets = 100;
  private final int countOfRecords = 20000;
  private String[] cities;
  private String[] airlines;

  public DbGenerate(String separator) {
    this.cities = DaoFile.read(Path.getPath() + "cities").split(separator);
    this.airlines = DaoFile.read(Path.getPath() + "airlines").split(separator);
  }

  private String getRandomData(String[] arrStr) {
    return arrStr[(int) (Math.random() * arrStr.length)];
  }

  private String getRecord(int index) {
    RandomDate rd = new RandomDate();
    return index + ","
        + rd.generateRandomDate() + ","
        + getRandomData(this.cities) + ","
        + getRandomData(this.airlines) + ","
        + (int) (Math.random() * this.maxCountOfTickets) + ";";
  }

  public void createFlightsFile() {
    StringBuilder flights = new StringBuilder();

    for (int i = 1; i <= this.countOfRecords; i++) {
      flights.append(getRecord(i));
    }
    DaoFile.write(Path.getPath() + "flights", flights.toString());
  }
}
